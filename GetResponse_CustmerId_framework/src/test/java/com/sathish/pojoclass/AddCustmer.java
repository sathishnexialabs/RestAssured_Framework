
package com.sathish.pojoclass;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class AddCustmer{

@SerializedName("id")
@Expose
private Integer id;
@SerializedName("custmerName")
@Expose
private String custmerName;
@SerializedName("Age")
@Expose
private Integer Age;

/**
* 
* @return
* The id
*/
public Integer getId() {
return id;
}

/**
* 
* @param id
* The id
*/
public void setId(Integer id) {
this.id = id;
}

/**
* 
* @return
* The custmerName
*/
public String getCustmerName() {
return custmerName;
}

/**
* 
* @param custmerName
* The custmerName
*/
public void setCustmerName(String custmerName) {
this.custmerName= custmerName;
}

/**
* 
* @return
* The Age
*/
public Integer getAge() {
return Age;
}

/**
* 
* @param Age
* The Age
*/
public void setAge(Integer Age) {
this.Age = Age;
}

}