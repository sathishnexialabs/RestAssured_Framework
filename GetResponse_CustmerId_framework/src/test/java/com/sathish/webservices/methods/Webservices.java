package com.sathish.webservices.methods;

import org.json.JSONException;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.authentication.AuthenticationScheme;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;

public class Webservices {
	public static String authToken;
	
	public static Response Post(String uRI,String stringJSON){
		RequestSpecification requestSpecification = RestAssured.given().body(stringJSON);
		requestSpecification.contentType(ContentType.JSON);
		Response response = requestSpecification.post(uRI);
		return response;
	}
	
	public static Response Get(String uRI){
		RequestSpecification requestSpecification = RestAssured.given();
		requestSpecification.contentType(ContentType.JSON);
		Response response = requestSpecification.get(uRI);
		return response;
	}

	
	public static void loginToApplication(String uRI, String userName, String password) throws JSONException{
		RequestSpecification requestSpecification = RestAssured.given().auth().form(userName, password);
		Response response = requestSpecification.get(uRI);
		org.json.JSONObject jsonObject = new org.json.JSONObject(response);
		String auth = jsonObject.getString("authToken");
		authToken = auth;
	}
}
