package com.sathish.testscripts;

import java.util.ArrayList;

import junit.framework.Assert;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.bhanu.pojoclass.AddCountry;
import com.bhanu.pojoclass.GetCountries;
import com.bhanu.utills.EndpointURL;
import com.bhanu.utills.URL;
import com.bhanu.utills.WebservicesUtil;
import com.bhanu.webservices.methods.Webservices;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jayway.restassured.response.Response;

public class TC_001 {
	Response response;
	ArrayList<String> custmer;
	ArrayList<Integer> Age;
	ArrayList<Integer> custmerid;
	
	@BeforeClass
	public void setUp(){
		custmer = new ArrayList<String>();
		age = new ArrayList<Integer>();
		custmerid = new ArrayList<Integer>();
		
	}


	//@Test(dataProvider="getCountryByID")
	public void verifyGetCountryById(Integer id, Integer Age, String custmer){
		Gson gson = new GsonBuilder().create();
		GetCustmer getCustmer;
		String url = URL.fixURL+EndpointURL.GET_CUSTMER_BY_ID.getResourcePath(id.toString());
		System.out.println(url);
		response = Webservices.Get(url);
		System.out.println(response.getBody().asString());
		if(response.getStatusCode() == 200){
			getCustemr = gson.fromJson(response.getBody().asString(), GetCustmer.class);
			Assert.assertEquals(id, getCustemr.getId());
			Assert.assertEquals(Age, getCustemr.getAge());
			Assert.assertEquals(custmer, getCustemr.getCustmerName());
		}
	}
	
	/**
	 * This data provider is used to validate custmer details
	 * @return
	 */
	@DataProvider(name="getCustmerByID")
	public Object[][] getCustmerByID(){
		Object[][] result = new Object[custmerid.size()][3];
		for(int i = 0; i<result.length; i++){
			result[i][0] = custmerid.get(i);
			result[i][1] = custmer.get(i);
			result[i][2] = age.get(i);
		}
		return result;
	}
	
	@DataProvider(name="addCustmer")
	public Object[][] addCountry(){
		Object[][] result = new Object[2][3];

		result[0][0] = "{\"id\":2,\"custmerName\":\"custmer1\",\"Age\":10}";
		result[0][1]="custmer1";
		result[0][2] = 2;

		result[1][0] = "{\"id\":3,\"custmerName\":\"custmer2\",\"Age\":69}";
		result[1][1] = "custmer2";
		result[1][2] = 3;
		return result;
	}
	
	@Test(dataProvider="addCustmer")
	public void verifyAddCountry(String json, String custmerName, int id){
		Gson gson = new GsonBuilder().create();
		AddCustmer addCustmer;
		String url = URL.fixURL+EndpointURL.ADD_CUSTMER.getResourcePath();
		response = Webservices.Post(url, json);
		if(response.getStatusCode() == 200){
			addCustmer = gson.fromJson(response.asString(), AddCustmer.class);
			Assert.assertEquals(custmerName, addCustmer.getCustmerName());
			Assert.assertEquals(new Integer(id), addCustmer.getId());
			
			//System.out.println(response.asString());
		}
	}
	
	@Test
	public void verifyUpdateCountry(){
		
	}
	
	@Test
	public void verifyDeleteCountry(){
		
	}

}
